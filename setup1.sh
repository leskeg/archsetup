#!/bin/bash

USERMANE="leskeg"

systemctl start NetworkManager.service

systemctl enable NetworkManager.service

useradd -m -g users -G audio,lp,optical,storage,video,wheel,games,power,scanner -s /bin/bash ${USERMANE}

passwd ${USERMANE}

sed -i '/'"%wheel ALL=(ALL) ALL"'/s/^#//' /etc/sudoers

# sudo nmcli dev wifi connect "SSID" password "password"

pacman -Syyu

pacman -Syy xorg xorg-xinit mesa mesa-demos xdg-user-dirs

su - ${USERMANE} -c "xdg-user-dirs-update"

multilib=`grep -n "\[multilib\]" /etc/pacman.conf | cut -f1 -d:`
sed -i "${multilib} s/^#//g" /etc/pacman.conf
((multilib++))
sed -i "${multilib} s/^#//g" /etc/pacman.conf
# printf '%s\n' '' '[archlinuxfr]' 'SigLevel = Never' 'Server = http://repo.archlinux.fr/$arch' >> /etc/pacman.conf

# pacman -Syy
# pacman -S yaourt

# video drivers

pacman -S xf86-video-vesa
#pacman -S xf86-video-intel
#pacman -S xf86-video-ati
#pacman -S xf86-video-nouveau mesa-libgl lib32-mesa-libgl

grub-mkconfig -o /boot/grub/grub.cfg

cp /etc/X11/xinit/xinitrc /home/${USERMANE}/.xinitrc

# kde 
# pacman -S plasma-meta sddm
# systemctl enable sddm
# echo "exec startkde" >> /home/${USERMANE}/.xinitrc

# xfce
pacman -S xfce4 lightdm lightdm-gtk-greeter
systemctl enable lightdm.service
echo "exec startxfce4" >> /home/${USERMANE}/.xinitrc

#amixer sset Master unmute

#reboot