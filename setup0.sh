#!/bin/bash

BOOT_PARTITION="/dev/sda1"
ROOT_PARTITION="/dev/sda2"
HOME_PARTITION="/dev/sda3"
SWAP_PARTITION="/dev/sda4"

MOUNTPOINT="/mnt"
KEYMAP="us"
LANG="en_US.UTF-8"
ZONE="America"
SUBZONE="New_York"
HOSTNAME="leskeg"

arch_chroot() {
    arch-chroot ${MOUNTPOINT} /bin/bash -c "${1}"
}

# Format partitions
mkfs -t ext2 ${BOOT_PARTITION}  # root
mkfs -t ext4 ${ROOT_PARTITION}  # /
# mkfs -t ext4 ${HOME_PARTITION}  # home
mkswap ${SWAP_PARTITION}       #swap
swapon ${SWAP_PARTITION}

# Mount partitions
mount ${ROOT_PARTITION} ${MOUNTPOINT}
mkdir ${MOUNTPOINT}/boot
mkdir ${MOUNTPOINT}/home
mount ${BOOT_PARTITION} ${MOUNTPOINT}/boot
mount ${HOME_PARTITION} ${MOUNTPOINT}/home

# Connect to wireless network
# wifi-menu

# PNG signatures update
dirmngr </dev/null
pacman-key --populate archlinux
pacman-key --refresh-keys

# System install
pacstrap ${MOUNTPOINT} base base-devel grub-bios networkmanager linux-lts xf86-input-synaptics wget

# Create fstab
genfstab -U -p ${MOUNTPOINT} >> ${MOUNTPOINT}/etc/fstab

# System configuration
# arch-chroot ${MOUNTPOINT}

# Localtime
arch_chroot "rm /etc/localtime"
arch_chroot "ln -s /usr/share/zoneinfo/${ZONE}/${SUBZONE} /etc/localtime"

# Hostname
arch_chroot "echo '${HOSTNAME}' >> /etc/hostname"

# TTY layout
arch_chroot "echo 'KEYMAP=${KEYMAP}' >> /etc/vconsole.conf"

# Language
arch_chroot "echo 'LANG=${LANG}' >> /etc/locale.conf"

# Locales
# nano /etc/locale.gen
arch_chroot "sed -i '/'${LANG}'/s/^#//' /etc/locale.gen"
arch_chroot locale-gen

# Grub install
arch_chroot "grub-install --target=i386-pc /dev/sda"
arch_chroot "grub-mkconfig -o /boot/grub/grub.cfg"
arch_chroot "mkinitcpio -p linux"
arch_chroot "mkinitcpio -p linux-lts"

# Root password
arch_chroot "passwd"

arch_chroot "cd /home && wget https://bitbucket.org/leskeg/archsetup/raw/master/setup1.sh"

# Umount partitions and reboot
umount ${MOUNTPOINT}/{boot,home}
reboot